(
s.options.memSize_(2.pow(16));

~out = 0;
~path = PathName(thisProcess.nowExecutingPath).parentPath ́++ "/git-samples/"; // path to sample directory
t = TempoClock(66/60).permanent_(true);

s.boot;
s.meter;
)

s.plotTree;

// allocate buffers
(
Buffer.freeAll;
s.newBufferAllocators;
)

~b1 = Buffer.read(s, ~path ++ "/acoustic/prog.aiff");

~b1.numFrames
~b1.bufnum

~guitar = {PlayBuf.ar(2, ~b1.bufnum, BufRateScale.kr(~b1.bufnum) * 1, 1, ~b1.numFrames * 0.2, 0, doneAction: 2)}

~guitar.play;

(
SynthDef(\sample, {
	arg buf = ~b1;
	var sig;
	sig = PlayBuf.ar(
		2,
		buf,
		BufRateScale.kr(buf) * \rate.kr(0.5, \ratelag.kr(0.075)),
		1,
		BufFrames.kr(buf) * \start.kr(0.0),
		\loop.kr(0),
		doneAction: 2);
	sig = sig * \amp.kr(0.75);
	Out.ar(~out, sig)
}).add;
)

x = Synth(\sample);
x = Synth(\sample, [\rate, 2.midiratio, \loop, 1]);
x.set(\rate, 1.midiratio);
x.free;


s.quit;
