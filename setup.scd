// a general-purpose setup file				
(
s.options.memSize_(2.pow(16));

~out = 0;
t = TempoClock(66/60).permanent_(true);

~makeBuses = {
	~bus = Dictionary.new;
	~bus.add(\reverb -> Bus.audio(s, 2));
};

~makeNodes = {
	~src = Group.new;
	~efx = Group.after(~src);

	~revSynth = Synth(\reverb, [
		\in, ~bus[\reverb],
		\out, ~out
	], ~efx);
};

~cleanup = {
	"see you!".postln;
	Window.closeAll;
	s.newBusAllocators;
	ServerBoot.removeAll;
	ServerTree.removeAll;
	ServerQuit.removeAll;
};

//////////// register functions
ServerBoot.add(~makeBuses);
ServerBoot.add(~makeNodes);
ServerQuit.add(~cleanup);

s.waitForBoot({
	s.plotTree;
	s.meter.window.alwaysOnTop_(true);

	s.sync;
		
SynthDef(\reverb, {
		var sig;
		sig = In.ar(\in.ir(0), 2);
		sig = JPverb.ar(
				sig, 
				\rtime.kr(4),
		\damp.kr(0.75),
		\size.kr(4.5),
		\earlyDiff.kr(0.8),
		\modDepth.kr(0.12),
		\modFreq.kr(2),
		\low.kr(1),
		\mid.kr(0.9),
		\high.kr(0.8)
	);
	sig = sig * \revAmp.kr(0.5);
	Out.ar(\out.ir(~out), sig);
}).add;

	ServerTree.add(~makeNodes);

	s.sync;
		s.freeAll;
	s.sync;

	"do eet".postln;
});
)

