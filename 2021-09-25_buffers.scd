(
s.options.memSize_(2.pow(16));

~out = 0;
~path = PathName(thisProcess.nowExecutingPath).parentPath ́++ "/git-samples/"; // path to sample directory
t = TempoClock(66/60).permanent_(true);

s.boot;
s.meter;
)

s.plotTree;

// allocate buffers
(
Buffer.freeAll;
s.newBufferAllocators;
)

~b1 = Buffer.read(s, ~path ++ "/acoustic/prog.aiff");

// get info about a buffer
~b1.server; // which server it lives on
~b1.bufnum; // number on the server
~b1.numFrames; // number of frames in the buffer
~b1.numChannels; // how many audio channels
~b1.sampleRate; // sample rate
~b1.path; // location of the file that is loaded in the buffer

// how long is the audio sample in the buffer?
~b1.numFrames / ~b1.sampleRate;
s.sampleRate; // sample rate of the server

~guitar = {PlayBuf.ar(2, ~b1.bufnum, BufRateScale.kr(~b1.bufnum) * 1, 1, ~b1.numFrames * 0.2, 0, doneAction: 2)}

~guitar.play;

// a synthdef
(
SynthDef(\sample, {
	arg buf = ~b1;
	var sig, env;
	env = Env.asr(\atk.ir(0.01), 1, \rel.ir(1.5)).kr(2, \gate.kr(1));
	sig = PlayBuf.ar(
		2,
		buf,
		BufRateScale.kr(buf) * \rate.kr(1, \ratelag.kr(0.075)),
		1,
		//Impulse.kr(2),
		//Dust.kr(1),
		BufFrames.kr(buf) * \start.kr(0.0),
		\loop.kr(0)
	);
	sig = sig * env * \amp.kr(0.75);
	Out.ar(~out, sig)
}).add;
)

x = Synth(\sample);
x = Synth(\sample, [\rate, -0.8, \start, 0.99]);
x.set(\rate, 2.midiratio);
x.set(\start, 0.5);
x.set(\loop, 1);
x.set(\gate, 0);
x.free;

s.quit;
