// wonky beat
(PathName(thisProcess.nowExecutingPath).parentPath ++ "setup.scd").load;

s.plotTree;

(	
SynthDef(\bling, {
	arg dry = 1;
	var sig, env, at, crv, freq, amp;
	freq = \freq.kr(220);
	amp = \amp.kr(-6.dbamp);
	at = \atk.kr(0.1) * freq.lincurve(30, 8000, 2, 0.25, -4);
	crv = \curve.kr(-2) * freq.linlin(30, 8000, 0.25, 2);
	env = Env.perc(at, \rel.ir(1.8), crv).kr(2);
	sig = SinOsc.ar(freq, mul: 0.5) + LFPulse.ar(freq * 0.999, 0, 0.4, 0.15);
	sig = sig * env * amp;
	sig = LPF.ar(sig, freq * 10);
	sig = Pan2.ar(sig, 0);
	Out.ar(\out.ir(~out), sig * dry);
		Out.ar(\fx.ir(0), sig * (1-dry));
}).add;

SynthDef(\bd, {
		arg dry = 1;
	var sig, env;
	env = Env.perc(0.0001, 1.2).kr(2);
	sig = AnalogBassDrum.ar(
		Impulse.kr(0),
		0,
		0.7,
		50,
		0.33,
		0.75,
		0.4,
		0.33
	);

	sig = Pan2.ar(sig * \amp.kr(0.95) * env, 0);
	Out.ar(\out.kr(~out), sig * dry);
	Out.ar(\fx.kr(~out), sig * (1-dry));
}).add;

SynthDef(\snare, {
		arg dry = 1;
	var sig, env;
	env = Env.perc(0.0001, 1.2).kr(2);
	sig = AnalogSnareDrum.ar(
		Impulse.kr(0),
		0,
		\accent.kr(0.2),
		\freq.kr(200),
		\tone.kr(0.7),
		\decay.kr(0.5),
		\snappy.kr(0.99)
	) * env * \amp.kr(0.5);
	sig = Pan2.ar(sig, 0);
	Out.ar(\out.kr(~out), sig * dry);
	Out.ar(\fx.kr(~out), sig * (1-dry));
}).add;

SynthDef(\gateRev, {
	var sig, in;
	in = In.ar(\in.ar(0), 2);
	sig = FreeVerb2.ar(
		in[0], in[1],
		\mix.kr(0.33),
		\room.kr(0.5),
		\damp.kr(0.75)
	);

	// Compander acts as a gate for the reverb
	sig = Compander.ar(
		sig,
		in,
		\thresh.kr(0.5),
		\slopeBelow.kr(10),
		\slopeAbove.kr(1),
		\clamp.kr(0.01),
		\relax.kr(0.1)
	);

	sig = sig.blend(in, \blend.kr(0.5));
	Out.ar(\out.kr(0), sig);
}).add;
)

Synth(\bling, [\freq, (Scale.minorPentatonic.degrees.choose + 56).midicps, \amp, -12.dbamp, \rel, 2, \dry, 0.5, \fx, ~bus[\reverb]], ~src);

~bus.add(\gateRev -> Bus.audio(s, 2));
~gateRevSynth = Synth(\gateRev, [\in, ~bus[\gateRev], \out, ~out], ~efx, \addToHead);

t.tempo_(80/60); // adjust tempo clock
// lengthen bar in tempo clock
t.schedAbs(t.nextBar, {t.beatsPerBar_(4.125)});
t.beatsPerBar; // check new length of the measure

// a simple bass drum pattern
(
~drum = Pbind(
	\instrument, \bd,
	\dur, Pseq([Pn(1, 3), 1.125], inf),
	\amp, 1,
	\dry, 0.75, 
	\group, ~src
);
)

~p1 = ~drum.play(t);
~p1.stop;

// simple snare
(
~snare = Pbind(
		\instrument, \snare,
		\type, Pseq([\rest, \note], inf),
		\dur, Pseq([Pn(1, 3), 1.125], inf), 
		\dry, 0.01,
		\amp, 0.5,
		\group, ~src
);
)

~p2 = ~snare.play;
~p2.stop;

// play them together
(
~dr = Ppar([
		~drum,
		~snare
]).play(t);
)
~dr.stop;

// a hihat - rest/note pattern inspired by my viewer S.
(
~hihat = Pbind(
		\instrument, \snare,
		\type, Pseq([\r, 
				Pn(\note, 2),
				\r,
				\note,
				Pn(\r, 2), 
				Pn(\note, 2),
				Pn(\r, 2),
				\note, 
				\r,
				Pn(\note, 2),
				\r
		], inf),
		\freq, 600,
		\dur, Pseq([
				Pn(0.5, 7), 
				0.625
		], inf), 
		\amp, Pwhite(0.3, 0.33), 
		\group, ~src,
		\dry, 0.75
);
)
~hi = ~hihat.play(t);
~hi.stop;

// play it together - really getting wonky!
(
~dr2 = Ppar([
		~drum,
		~snare,
		~hihat
]).play(t);
)
~dr2.stop;

// slow it down	
t.tempo_(60/60);

// background noise 
c = {Crackle.ar(1.99, 0.05)!2}.play(outbus: ~bus[\reverb]);
c.release;

// good night!
s.quit;
