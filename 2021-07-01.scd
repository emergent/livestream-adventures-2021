// Livestream 2021-06-24: granular stuff & composition ctd.

// possible other things: pattern from last time, but as Pbindef with fadeTime manipulation

// make an fx chain (delay, lowpass, shimmer verb?)

(
~out = 0; // global variable for output (this is not necessary at this point, but good style and might come in handy if you start working with a system that has several outputs & don't want to change it everywhere you used it in the code)
~fund = 39; // fundamental note of the composition as midinote; midinote 39 = Eb2

s.options.memSize_(2.pow(20)); // generous memory allocation
s.boot;
s.meter;
)

s.plotTree; // visualize server tree

{PinkNoise.ar(0.4!2)}.play;

(
t = TempoClock(72/60).permanent_(true); // tempo clock @ 72 bpm
// make groups

~src = Group.new;
~efx = Group.after(~src);

// free any buses that might be lingering on the server unused
s.newBusAllocators;

// bus dictionary
~bus = Dictionary.new;
~bus.add(\reverb -> Bus.audio(s,2)); // reverb bus
~bus.add(\pitch -> Bus.audio(s,2)); // pitchshift bus
)

~bus[\reverb].scope;

///////////////////////////////////// SynthDefs /////////////////////////////
(
SynthDef(\sawCluster, {
	var sig, env, dry;
	dry = \dry.kr(1);
	env = Env.new( // custom envelope
		[0, 1, 0.8, 0],
		[\atk.ir(1.2), \sus.ir(2), \rel.ir(8)],
		[\sine, \exp, -4]
	).kr(2);
	sig = 5.collect{
		VarSaw.ar(
			\freq.kr(220) * LFNoise1.kr(0.18).bipolar(\detune.kr(0.125)).midiratio,
			{rrand(0.0, 2pi)}, // random initial phase
			{rrand(0.001,0.05)}, // slightly randomized width, but staying pretty narrow
			0.2
	)};
	sig = sig.blend(sig.distort, \blend.kr(0.25)); // blend in a bit of distortion
	sig = sig * env * \amp.kr(0.9);
	sig = Splay.ar(sig);
	Out.ar(\out.ir(~out), sig * dry);
	Out.ar(\fx.ir(~bus[\reverb]), sig * (1-dry));
}).add;

// fun with Ringz
SynthDef(\ring, {
	var sig, env, dry;
	dry = \dry.kr(1);
	env = Env.perc(\atk.kr(0.01), \rel.kr(4)).kr(2);
	sig = Ringz.ar(
		Impulse.ar(\pulse.kr(4), 0, 0.25),
		\freq.kr(330),
		\ringtime.kr(1)
	);
	sig = sig * env * \amp.kr(0.5);
	sig = Pan2.ar(sig, \pan.kr(0));
	Out.ar(\out.ir(~out), sig * dry);
	Out.ar(\fx.ir(~bus[\reverb]), sig * (1-dry));
}).add;

// a sawtooth wave pulse
SynthDef.new(\sawPulse, {
	var sig, env;
	env = Env.asr(\atk.ir(0.075), 1, \rel.ir(1)).kr(2, \gate.kr(1));
	sig = VarSaw.ar(\freq.ar(220), 0.2,
		\width.kr(0.05) + LFNoise1.kr(0.25).bipolar(0.07),
		LFPulse.kr(\pulsehz.kr(4), 0.0, \pulsewidth.kr(0.45), 0.4)
	)!2;
	sig = sig * env * \amp.kr(0.5);
	sig = Balance2.ar(sig[0], sig[1], \pan.ar(0));
	Out.ar(\out.ir(~out), sig * \dry.kr(1));
	Out.ar(\fx.ir(~bus[\reverb]), sig * (1-\dry.kr(1)))
}).add;

// FM bell
SynthDef.new(\fmbell, {
	var car, mod, env, iEnv, vib, dry, freq, atk, rel, cAtk, cRel, index;
	atk = \atk.kr(0.01);
	rel = \rel.ir(1);
	cAtk = \cAtk.ir(4);
	cRel = \cRel.ir(-4);
	freq = \freq.kr(440);
	index = \index.kr(1);
	dry = \dry.kr(1);
	iEnv = EnvGen.kr(
		Env.new(
			[index, index * \iScale.ir(5), index],
			[atk, rel],
			[cAtk, cRel]
		));
	env = Env.perc(atk, rel, curve:[cAtk, cRel]).kr(2);
	env = env * PinkNoise.ar(1).range(0.1, 1).lag(0.02); // envelope multiplied with PinkNoise to make it a little "dirtier"; lower lag values = more audible noise
	vib = SinOsc.ar({exprand(2.0,5.0)}, 0, freq * {rand(0.015, 0.02)});
	mod = SinOsc.ar(freq * \mRatio.kr(1), mul:freq * \mRatio.kr(1) * iEnv);
	car = SinOsc.ar(freq * \cRatio.kr(1) + mod + vib) * env * \amp.kr(-14.dbamp);
	car = HPF.ar(car, \hpcut.kr(200));
	car = Pan2.ar(car, \pan.kr(0));
	Out.ar(\out.ir(~out), car * dry);
	Out.ar(\fx.ir(~bus[\reverb]), car * (1-dry));
}).add;

// sinewave "bling"

SynthDef(\bling, {
	var sig, env, dry;
	dry = \dry.kr(0.5);
	env = Env.perc(\atk.kr(0.001), \rel.kr(0.25), 1, \curve.kr(-2)).kr(2);
	sig = SinOsc.ar(\freq.kr(330))!2;
	sig = sig * env * \amp.kr(-2.dbamp);
	Out.ar(\out.kr(~out), sig * dry); // dry signal
	Out.ar(\fx.kr(~bus[\reverb]), sig * (1 - dry)); // wet signal
	Out.ar(\fx2.kr(~bus[\pitch]), sig * \send.kr(0.0)); // "send" out w/ independent volume
}).add;

// granular "sparkles"

e = Env.new([0, 1, 0],[0.005, 0.3],[0, -4]); //percussive envelope
~grainEnv = Buffer.sendCollection(s, e.discretize(4096));

SynthDef(\sparkle, {
	var sig, freq, freq1, env, dry, grainfreq, pan;
	freq = \freq.kr(440);
	pan = LFNoise1.kr(0.25).bipolar(\panwidth.kr(0.9));
	dry = \dry.kr(1);
	grainfreq = \grainfreq.kr(4);
	freq1 = LFNoise0.kr(grainfreq).exprange(freq, freq*\maxPartial.kr(12)).round(freq/2);
	env = Env.perc(\atk.ir(0.001), \rel.ir(5), 1, \curve.ir(-2)).kr(2);
	sig = GrainSin.ar(
			2,
			Dust.ar(grainfreq),
			\graindur.kr(0.05),
			freq1,
			pan,
			\envbuf.ir(~grainEnv),
			512, // cannot be modulated
			\amp.kr(-12.dbamp)
	) * env;
	Out.ar(\out.kr(~out), sig * dry);
	Out.ar(\fx.kr(~bus[\reverb]), sig * (1 - dry));
	Out.ar(\fx2.kr(~bus[\pitch]), sig * \send.kr(0.0)); // "send" out w/ independent volume
}).add;

///////////////////////// Effects ////////////////////////////////

// Pitchshift
SynthDef.new(\shift, {
	var sig;
	sig = PitchShift.ar(
		In.ar(\in.kr(~bus[\pitch]), 2),
		\winSize.kr(0.2),
		\pitchRatio.kr(2),
		\pitchDisp.kr(0),
		\timeDisp.kr(0),
		\amp.kr(-1.dbamp)
	);
	Out.ar(\out.kr(~bus[\reverb]), sig);
}).add;

// reverb
SynthDef.new(\reverb, {
	var sig;
	sig = JPverb.ar(
		In.ar(\in.ir(~bus[\reverb]), 2),
		\rtime.kr(4),
		\damp.kr(0.75),
		\size.kr(4.5),
		\earlyDiff.kr(0.8),
		\modDepth.kr(0.12),
		\modFreq.kr(2),
		\low.kr(1),
		\mid.kr(0.9),
		\high.kr(0.8)
	);
	sig = sig * \revAmp.kr(0.5);
	Out.ar(\out.kr(~out), sig);
}).add;
)

// instantiate reverb
~reverb = Synth(\reverb, [\in, ~bus[\reverb], \out, ~out], ~efx, addAction:\addToTail);
~reverb.set(\rtime, 8);
// fool around with fm synth - atk and rel values can totally change a sound; so can the values mRatio, cRatio, iScale and index; perhaps make a pattern!

~pitch = Synth(\shift, [\in, ~bus[\pitch], \out, ~bus[\reverb], \pitchDisp, 0.001, \timeDisp, 0.001], ~efx);

(
y = Synth(\fmbell, [
	\freq, 51.midicps,
	\amp, -24.dbamp,
	\atk, 0.001,
	\rel, 4,
	\iScale, 1,
	\cAtk, 1,
	\cRel, -4,
	\mRatio, pi/2,
	\cRatio, 5.sqrt,
	\index, 2.sqrt,
	\dry, 1,
	\fx, ~bus[\reverb],
	\out, ~out
], ~src);
)

// single synth, fooling around with the values
(
z = Synth(\sparkle, [
	\freq, rrand(63,75).midicps,
	\amp, rrand(-24,-20).dbamp, // reduce amplitude at high grain durations
	\maxPartial, 16,
	\atk, 0.001,
	\rel, 8,
	\curve, -5,
	\grainfreq, 12,
	\graindur, 1,
	\envbuf, -1, // built-in "hanning window" grain envelope
	\envbuf, ~grainEnv, // custom percussive envelope
	\send, 0.01,
	\dry, 0.7
], ~src);
)



///////////////////////////////// Patterns! //////////////////////////////////

(
~sawclust = Pbind(
	\instrument, \sawCluster,
	\midinote, Pfunc({
			5.collect{
					(Scale.dorian.degrees.choose + ~fund) + [0, 12, 24, 36].choose
			};
	}),
	\amp, Pwhite(0.025, 0.05),
	\blend, Pwhite(0.1, 0.9),
	\detune, Pwhite(0.01,0.2),
	\atk, Pwhite(0.5, 2),
	\sus, Pwhite(0.01, 3),
	\dur, Pexprand(4.0, 6.0),
	\out, ~out,
	\dry, 0.6,
	\group, ~src
);
)

// play it
~player1 = ~sawclust.play(t, quant:1);
~player1.stop; // stop the stream

(
~gongs = Pbind(
	\instrument, \fmbell,
	\midinote, Pxrand((Scale.dorian.degrees + ~fund + 12), 8),
	\dur, Pwhite(6.0, 8.0),
	\atk, 0.001,
	\rel, Pwhite(3.2, 8.0),
	\iScale, Pwhite(1, 4),
	\cAtk, 1,
	\cRel, Pwhite(-6, -2),
	\mRatio, Pwrand([1.2, 2.sqrt, pi/2, pi], [1,2,4,2].normalizeSum, inf),
	\cRatio, Pwrand([1, 2.sqrt, 5.sqrt],[0.5, 1, 2].normalizeSum, inf),
	\index, Prand([1, 1.66, pi], inf),
	\dry, Pwhite(0.25, 0.6),
	\amp, Pwhite(-24.dbamp,-20.dbamp),
	\fx, ~bus[\reverb],
	\out, ~out,
	\group, ~src
);
)

~player2 = ~gongs.play(t, quant:1);
~player2.stop;

// figuring out a combination of Pstutter and Pseries
(
a= Pstutter(4,Pseries(24, -1, 16)).asStream;
a.next;
)

// array pendant to Pgeom
Array.geom(16, 0.01, 1.36).mirror

// figuring out Pgeom
(
p = Pseq([Pgeom(0.01, 1.35, 16), Pgeom(0.9, 1/1.35, 16)]).asStream;
16.do({ |i| p.next.postln; });
)

// transforming the sparkles synth fooling into a Pbind
(
~sparkle1 = Pbind(
	\instrument, \sparkle,
	\midinote, Pxrand((Scale.dorian.degrees + ~fund + [24,36].choose), inf),
	\dur, 4,
	\atk, 0.1,
	\rel, 3.6,
	\curve, -4,
	\maxPartial, Pseries(24, -1, 16),
	\graindur, Pseq([Pgeom(0.01, 1.35, 16), Pgeom(0.9, 1/1.35, 16)]),
	\envbuf, ~grainEnv,
	\amp, Pwhite(-24.dbamp,-20.dbamp),
	\send, Pwhite(0.25, 0.3),
	\dry, Pwhite(0.4, 0.45),
	\group, ~src
);
)

~player4 = ~sparkle1.play(t, quant:1);
~player4.stop;















////////////// let's get composing! //////////////////////

(
~intro = Pbindf(~sawclust, \amp, Pseg(
	Pseq([0.01, 0.2, 0.001], 1),
	Pseq([24, 8, 12], 1),
	Pseq([2, 0, -1], 1)
));
)
~event1 = ~intro.play(t);


// 3 instances of the ringz instrument with randomized parameters

(
~event2 = Routine({
	3.collect{
		Synth(\ring, [
			\freq, (Scale.dorian.degrees.choose+(~fund+36)).midicps,
			\pulse, rrand(1, 4),
			\pan, rrand(-1.0, 1.0),
			\ringtime, rrand(0.4, 2),
			\atk, rrand(0.1, 0.5),
			\rel, rrand(6, 12)
		],
		~src
		)
	};

	10.wait;
}).repeat(3);
)

~event2.play(t);
~event2.reset;

// a bunch of the sawPulse instruments with randomized parameters in a routine
(
~event3 = Routine({
	(4..8).choose.do{
	Synth(\sawPulse,
		[
			\freq, (Scale.dorian.degrees+~fund).midicps.choose,
			\pulsehz, rrand(2,8),
			\pulsewidth, rrand(0.2,0.75),
			\pan, rrand(-1.0, 1.0),
			\amp, exprand(0.1, 0.3),
			\atk, exprand(0.01, 0.3),
			\rel, rrand(0.75, 2),
			\out, ~out,
			\dry, 0.5,
			\fx, ~fx,
	], ~src)
	};

	rrand(4.0,8.0).wait;

	~src.set(\gate, 0);

	rrand(0.5, 2.0).wait;

}).repeat(2);
)

~event3.play(t);
~event3.stop;
~event3.reset;

s.freeAll;
s.quit;
