# Livestream adventures 2021

SuperCollider files that I write during my weekly livestreams at https://www.twitch.tv/the_emergent

These files are intended for information, education and entertainment. I do my best to check if the code is working as intended before publication; however, I cannot give any guarantees that the information they contain is complete and accurate, nor can I guarantee that they will produce the intended results on your machine.

Use this code at your own risk.
